## Canvas / Slack Group Number
  Group 11
## Team Members
Sugandha Kumar, Adithya Bhonsley, Benjamin Sandoval, Anna Jimenea, Jean-Claude Bissou
## Project Name
  SupportSouthSudan
## Project Description
SupportSouthSudan aims to create an understanding of the South Sudanese refugees, including the countries that have accepted them, associated charities/organizations, and related news articles. The goal is to increase awareness about the refugees' experiences, highlight the crucial role played by charities in assisting their community, and provide up-to-date information on the country's refugee situation.

## Data Sources
* UNHCR ODP https://data.unhcr.org/en/situations/southsudan
* ReliefWeb https://reliefweb.int/updates?view=headlines&search=south+sudan+refugee
* UNHCR Data Finder https://www.unhcr.org/refugee-statistics/download/?url=jwA88D
* UNHCR ODP API https://data.unhcr.org/api/doc#/
* REST Countries https://restcountries.com/
* News API https://newsapi.org/

## Models
* Countries
* News
* Charities

## Estimated Number of Instances per Model
* Countries: 55
* News: 644
* Charities: ~100

## Attributes for Each Model
* Countries:
  * Name
  * Pop. of accepted refugees from South Sudan
  * Pop. of asylum-seekers from South Sudan
  * Total Population
  * Distance from South Sudan? (Likely)

* News:
	* Article name
	* Publishing date
	* News Source
	* Main topic(s)? (Probable)
	* Country of origin/importance
	* Type of article (press release, analysis, etc.)
	
	
* Charities:
	* Name
	* Type of assistance (Food, medical, education, etc.)
	* Location
	* Establishment date
	* Donations/volunteers
	* Amount (monetarily) contributed
	
	

## Model Connections
* Countries - News: News articles involving this country
* Countries - Charities: Countries that the charities originate from
* News - Country: What country the news takes place in
* News - Charities: Charities that serve as the source of the article
* Charities - Countries: Countries the charities originate from
* Charities - News: News articles that cover the same topic this charity seeks to help with


## Media for Each Model
* Countries:
  * Map (showing distribution) or graph
  * Text
* News:
  * Image/News Clipping
  * Text
* Charities:
  * Image (Flyer/Poster)
  * Video 
  * Text

## Questions Our Website Will Answer
* What is currently happening in South Sudan as of recently?
* What countries do people who flee from South Sudan seek asylum?
* What resources or organizations can provide support for South Sudanese people once they find asylum?
